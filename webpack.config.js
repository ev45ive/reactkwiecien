var path = require('path')
var HTMLWebpackPlugin = require('html-webpack-plugin')
var ExtractText = require('extract-text-webpack-plugin')

module.exports = {
    entry:{
        //vendor: ['react'],
        main:'./src/main.js'
    },
    output:{
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    devtool: 'sourcemap',
    resolve:{
        extensions:['.jsx','.js']
    },
    module:{
        rules:[
            { test:/\.jsx?$/, use: 'babel-loader' }, 
            { test:/\.css$/, use: ExtractText.extract({
                use:'css-loader',
                fallback:'style-loader'
            })},
            { test:/\.(jpg|png|svg)$/, use:{
                loader:'file-loader',
                options:{
                    name: 'assets/[name].[hash].[ext]'
                }
            }}
        ]
    },
    plugins:[
        new ExtractText('style.css'),
        new HTMLWebpackPlugin({
            template: './src/index.html'
        })
    ],
    devServer:{
        historyApiFallback:true
    }

}