import {createStore, applyMiddleware} from 'redux'
import {rootReducer, initialState} from '../reducers/root.reducer'
import {addTodo,removeTodo} from '../actions/action_creators'
import {composeWithDevTools} from 'redux-devtools-extension'


const loggerMiddleware = store => next => action => {
    console.log('action',action);
    const state = next(action)    
    console.log('state:',state)
    return state;    
};
// redux-thunk
// redux-promise + (redux-actions)
const asyncMiddleware = store => next => action => {

    if('function' === typeof action){
        action( store.dispatch );  
        return
    }
    return next(action)
};

// function asyncMiddleware(store){
//     return function runCurrent(next){
//         return function runAction(action){
//             let state = next(action)
//             return state;
//         }
//     }
// }

export const store = createStore(rootReducer, initialState,
composeWithDevTools(applyMiddleware(
    loggerMiddleware,
    asyncMiddleware
)));
