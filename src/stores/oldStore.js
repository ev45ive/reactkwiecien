
import { Store } from './stores/FluxStore'

const ADD_TODO = "ADD_TODO"

const store = new Store((state, action) => {
    switch (action.type) {
        case ADD_TODO:
            state.todos = [
                ...state.todos,
                {
                    id: Date.now(),
                    title: action.payload.title,
                    completed: false
                }
            ]
            break;
        case 'TOGGLE_TODO': {
            let todoId = action.payload.id;
            let todoIndex = state.todos.findIndex(({ id }) => id == todoId)
            let todo = state.todos[todoIndex]
            let newTodo = Object.assign({}, todo, { completed: !todo.completed });
            state.todos = [
                ...state.todos.slice(0, todoIndex),
                newTodo,
                ...state.todos.slice(todoIndex + 1)
            ]
        } break;
        case 'FETCH_USERS_LOAD':
            state.users = action.payload.users;
            break;
    }
}, {
        todos: [
            { id: 1, title: 'Zakupy', completed: true },
            { id: 2, title: 'Nauczyć się Reacta!', completed: false }
        ],
        users: [
            { id: 1, username: 'Testowy' }
        ],
        items:{
            data:[],
            pending:false,
            error:null,
            lastFetch: 0,
        }
    })