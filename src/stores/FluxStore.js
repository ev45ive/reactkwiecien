import EventEmitter from 'events'

const EVENT_NAME = 'update'

export class Store extends EventEmitter{

    constructor(actionHandler, initialState = {}){
        super()
        this.state = initialState
        this.actionHandler = actionHandler;
    }

    dispatch(action){
        this.actionHandler.apply(this,[this.state,action]);
        this.emit(EVENT_NAME, this.state )
    }

    getState(){
        return this.state;
    }

    subscribe(callback){
        this.on(EVENT_NAME, callback)
    }

    unsubscribe(callback){
        this.removeListener(EVENT_NAME, callback)
    }
}