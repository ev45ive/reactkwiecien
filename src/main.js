import React from 'react'
import ReactDOM from 'react-dom'
import {App} from './app'
import {Layout} from './components/Layout'
import 'bootstrap/dist/css/bootstrap.css'

import {store} from './stores/redux.store'
import {Provider} from 'react-redux';

import {BrowserRouter as Router, Route} from 'react-router-dom'

ReactDOM.render(
<Router>
    <Provider store={store}>
        <Layout title="Witaj w Webpack! ">
            <App/>    
        </Layout>
    </Provider>
</Router>, document.getElementById('app-root'))
