import React from 'react'
import { Route, Redirect, withRouter } from 'react-router-dom'
import { Todos } from './containers/todos'
import { Users } from './containers/users'
import { User } from './containers/user'

//const RoutedUsers = withRouter(Users)

const UsersView = props => <div className="row">
    <div className="col">
        <Route path="/users" component={Users} />
    </div>

    <div className="col">
        <Route path="/users/:id" component={User} />
        <Route path="/users" exact render={
            ()=><div>Select user</div>
        } />
    </div>
</div>

export const App = props => <div className="col">
    <Route path="/" exact render={()=><Redirect to="/todos" />} />
    <Route path="/todos" component={Todos} />
    <Route path="/users" component={UsersView} />
</div>