import React from 'react'
import {arrayOf,object,string,func} from 'prop-types'

const updateCounter =  fieldName => (prevState, props) => {
    return {
        counter: prevState[fieldName].length
    }
}

export class Todos extends React.PureComponent {

    constructor() {
        super()
        this.state = {
            newTitle: "Nowy Todo",
            counter: 0,
        }
    }

    static propTypes = {
        title: string.isRequired,
        todos: arrayOf(object),
        toggleCompleted: func.isRequired,
        addTodo: func.isRequired
    }

    static defaultProps = {
        title: 'Todos',
        todos: []
    }
    onTitleChange = (event) => {
        //event.persist()
        let newTitle = event.target.value;
        this.setState((prevState, props) => {
            return {
                newTitle
            }
        })
    }

    onEnter = event => {
        if(event.keyCode == 13){
            this.onAdd()
            return;
        }
    }
    

    onAdd = event => {
        this.props.addTodo(this.state.newTitle)
        this.setState({ newTitle: '' })
    }

    render() {
        return <div>
            <h4>{this.props.title}</h4>
            <ul className="list-group">
                {this.props.todos.map(todo =>
                    <li 
                    style={{
                        textDecoration: todo.completed? 'line-through':''
                    }}
                    onClick={ e => this.props.toggleCompleted(todo.id)} className="list-group-item" key={todo.id}>{todo.title}</li>
                )}
            </ul>

            <div className="input-group">
                <input className="form-control"
                ref={ elem => elem && elem.focus() }
                 onKeyUp={this.onEnter}
                 onChange={this.onTitleChange} value={this.state.newTitle} />
                <div className="input-group-addon">{this.state.newTitle.length}</div>
                <button onClick={ this.onAdd }>Dodaj</button>
            </div>
        </div>
    }
}