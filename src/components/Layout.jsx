import React from 'react'
import {Link} from 'react-router-dom'


export const Layout = props => <div>
    <nav className="navbar navbar-light bg-faded">
        <div className="container">
            <ul className="navbar-nav">
                <li className="nav-item">
                    <Link className="link-link" to="/todos">Todos</Link>
                </li>
                <li className="nav-item">
                    <Link className="link-link" to="/users">Users</Link>
                </li>
            </ul>
        </div>
    </nav>
    
    <div className="container">
        <div className="row">
            <div className="col">
                <h1> {props.title} </h1>
                {props.children}
            </div>
        </div>
    </div>
</div>