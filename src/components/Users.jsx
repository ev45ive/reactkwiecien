import React from 'react'
import propTypes from 'prop-types'

/*const IfLoggedIn = (props,context) => <div>
    {context.loggedIn? props.children : null}
</div>

<IfLoggedIn>
    <Admin/>
</IfLoggedIn>*/

const UserListItem = props => <li 
    onClick={e=> props.onSelect(props.user)} className="list-group-item">
    {props.user.id}.
    {props.user.username}
</li>

export class Users extends React.Component {

    constructor() {
        super()
    }
    static defaultProps = {
        Item: UserListItem,
        users: [],
        pending: false
    }

    select = user => {
        this.props.history.push('/users/'+user.id);
    }

    render() {
        const Item = this.props.Item;
        return <div>
            <h4> Users </h4>
            <button onClick={this.props.fetchUsers}>Fetch</button>
            {!this.props.pending ? <ul className="list-group">
                {this.props.users.map(user =>
                    <Item key={user.id} user={user} onSelect={this.select}>
                    </Item>
                )}
            </ul> : <div>Please wait...</div>}
        </div>
    }
}