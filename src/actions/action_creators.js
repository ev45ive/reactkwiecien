
export const addTodo = title => ({
    type: 'ADD_TODO',
    payload: {
        todo: {
            id: Date.now(),
            title,
            completed: false
        }
    }
});
export const removeTodo = id => ({
    type: 'REMOVE_TODO',
    payload: {
        id
    }
});
export const toggleCompleted = id => ({
    type: 'TOGGLE_TODO',
    payload: {
        id
    }
});


function getData(path) {
    return fetch('http://localhost:3000/' + path)
        .then(response => {
            if (response.ok) {
                return response.json()
            } else {
                throw Error(response.statusText)
            }
        })
}


export const fetchUsers = () => {

    // return getData('users')
    //     .then( users => ({
    //         type:'FETCH_USERS_SUCCESS',
    //         payload:{
    //             users
    //         }
    //     }))

    // return ({
    //     type:'FETCH_USERS',
    //     payload: getData('users')
    // })

    // return ({
    //     type:'FETCH_USERS',
    //     status:'success',
    //     payload: [1,2,3]
    // }

    return function (dispatch) {
        dispatch({
            type: 'FETCH_USERS_START'
        })
        return getData('users')
            .then(users => {
                dispatch({
                    type: 'FETCH_USERS_LOAD',
                    payload: { users }
                })
            })
            .catch(() => {
                dispatch({
                    type: 'FETCH_USERS_ERROR'
                })
            })
    }
}