import React from 'react';
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux';
import {Todos as TodosComponent} from '../components/Todos'
import {addTodo,toggleCompleted} from '../actions/action_creators'
import { createSelector } from 'reselect'

const selectTodos = createSelector(
    state => state.todos.index, //oldIndex === index
    state => state.todos.order,
    (index, order) => order.map(id => index[id] )
)


const stateToProps = state => ({
    todos: selectTodos(state)
})

// const dispatchToProps = dispatch => ({
//     toggleCompleted: id => dispatch(toggleCompleted(id)),
//     addTodo: title => dispatch(addTodo(title))
// })

const dispatchToProps = dispatch => bindActionCreators({
    toggleCompleted,
    addTodo
}, dispatch);


const connector = connect(stateToProps,dispatchToProps)

// Component connected to Store:
export const Todos = connector(TodosComponent);
