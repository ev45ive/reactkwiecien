import React from 'react';
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux';
import {User as UserComponent} from '../components/User'
import {fetchUsers} from '../actions/action_creators'

const stateToProps = state => ({
    index: state.users.index
})

const dispatchToProps = () => {
    return {}
}

const mergeProps = (stateProps, dispatchProps, props) => ({
    user: stateProps.index[ parseInt(props.match.params.id) ]
})

const connector = connect(stateToProps,dispatchToProps,mergeProps)

// Component connected to Store:
export const User = connector(UserComponent);
