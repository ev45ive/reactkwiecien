import React from 'react';
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux';
import {Users as UsersComponent} from '../components/Users'
import {fetchUsers} from '../actions/action_creators'

const stateToProps = state => ({
    users: state.users.items.map( id => state.users.index[id] )
})

// const dispatchToProps = dispatch => ({
//     toggleCompleted: id => dispatch(toggleCompleted(id)),
//     addTodo: title => dispatch(addTodo(title))
// })

const dispatchToProps = dispatch => bindActionCreators({
    fetchUsers: fetchUsers
},dispatch);

const mergeProps = (stateProps, dispachProps, props) => ({
    ...stateProps,
    ...dispachProps,
    ...props,
})

const connector = connect(stateToProps,dispatchToProps,mergeProps)

// Component connected to Store:
export const Users = connector(UsersComponent);
