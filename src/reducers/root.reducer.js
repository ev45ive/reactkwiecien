import { combineReducers } from 'redux'

export const counterReducer = (state = 0, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1;
    case 'DECREMENT':
      return state - 1;
    default:
      return state;
  }
};

export const todosReducer = (state = {}, action) => {
  switch (action.type) {
    case 'ADD_TODO':{
      let todo = action.payload.todo;
      return {
        ...state,
        index: { ...state.index, [todo.id]:todo },
        order: [ ...state.order, todo.id ],
      }
    }
    case 'REMOVE_TODO':
      let todoIndex = state.index.indexOf(action.payload.id)
      return {
        ...state,
        order:[
          ...state.order.slice(0, todoIndex),
          ...state.order.slice(todoIndex + 1)
        ]
      }
    case 'TOGGLE_TODO': {
        let id = action.payload.id;
        let todo = state.index[id];
        return {
          ...state,
          index:{...state.index, [id]:{...todo, completed:!todo.completed }}
        }
      return state;
    }
    default:
      return state;
  }
}
export const usersReducer = (state = {}, action) => {
  switch (action.type) {
    case 'FETCH_USERS_LOAD':
      let users = action.payload.users;
      return {
        ...state,
        items: users.map( user => user.id ),
        index: users.reduce((index,user) => {
          index[user.id] = user;
          return index;
        }, state.index)      
      };
    default:
      return state;
  }
};

export const initialState = {
  counter: 0,
  todos: {
    index:{
      123:{id:123, title:'todo 123'},
      234:{id:234, title:'todo 234'}
    },
    order:[
      123,234
    ]
  },
  users: {
    items: [1],
    index:{
      1: { id: 1, username: 'Testowy' }
    }
  }
}

// export const rootReducer = (state = initialState,action) =>  ({
//     ...state,
//     counter: counterReducer(state.counter, action),
//     todos: todosReducer(state.todos,action),
//     users: usersReducer(state.users, action)
//        users:{
//          items: usersReducer(state.users.items,action)
//        }
// });

export const rootReducer = combineReducers({
  counter: counterReducer,
  todos: todosReducer,
  users: usersReducer,
})

// https://bitbucket.org/ev45ive/reactkwiecien
//git clone https://ev45ive@bitbucket.org/ev45ive/reactkwiecien.git nazwa_katalogu


// var makeReducersWithActions = ( LIST_NAME ) => ({
//     reducer: (state = [], action) => {
//         switch(action.type){
//         case 'ADD_'+LIST_NAME:
//             return [...state, action.payload.item];
//         case 'REMOVE_'+LIST_NAME:
//             let itemIndex = state.findIndex( ({id})=> id == action.payload.id )
//             return  [
//                 ...state.slice(0, itemIndex),
//                 ...state.slice(itemIndex + 1)
//             ];
//         default:
//             return state;
//         }
//     },
//     actions:{
//         addItem: item => ({ type: ('ADD_'+LIST_NAME), payload: { item }}),
//     }
// })